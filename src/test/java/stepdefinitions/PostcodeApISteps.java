package stepdefinitions;

import io.restassured.RestAssured;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

public class PostcodeApISteps {
 private int status;
    @Step
    public void searchPostcode(String postcode){
        status = RestAssured.get("http://api.postcodes.io/postcodes/"+postcode).statusCode();
        RestAssured.
                when().get("http://api.postcodes.io/postcodes/"+postcode).
                then().statusCode(200);
    }

    @Step
    public void verifyStatusCode(int statusCode){
        Assert.assertEquals(200,statusCode);
    }
}
